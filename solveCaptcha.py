from guessCaptcha import guessFunc
import requests
import time, datetime
import random
import re


#***************************************
#HELPERS
#***************************************

def jsGettime():

    rnd = datetime.datetime.now()
    t = str(int(time.mktime(rnd.timetuple())))
    ms = str(int(rnd.microsecond * 0.001))
    return t + ms

#Returns a string from html that's located between a front string and an end string
def getString(html, front, end):

    regex = front + '(.+?)' + end
    #scraper_log(regex)
    pattern = re.compile(regex)

    try:

        string = str(re.findall(pattern, html)[0])

    except:

        string = 'Inconnu(e)'

    #Clean string
    string = string.strip()

    return string

#***************************************
#REQUESTS
#***************************************
#Get the CSRFToken

def get_csrftoken(s):

    url = 'https://checkcoverage.apple.com/'

    proxies = []

    proxy = '' #random.choice(proxies)

    headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                 'Accept-Encoding': 'gzip,deflate,sdch',
                 'Accept-Language': 'en-US,en;q=0.8,fr;q=0.6',
                 'Cache-Control':'max-age=0',
                 'Connection': 'keep-alive',
                 'X-Requested-With': 'XMLHttpRequest',
                 'Host': 'checkcoverage.apple.com',
                 #'Cookie': 's_orientationHeight=693; POD=us~en; s_vnum_n2_us=4|2; s_vi=[CS]v1|2B12C8FB05010C79-600016026009BB12[CE]; s_cc=true; s_fid=362FB5AA2365003B-0D3EA56E7E41217E; s_ppv=acs%253A%253Atools%253A%253Awck%253A%253A1-enter%2520serial%2520number%2520%2528US%2529; s_orientation=%5B%5BB%5D%5D; s_pathLength=support%3D3%2C; s_sq=%5B%5BB%5D%5D',
                 'Upgrade-Insecure-Requests':'1',
                 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36'}

    #print "Sending request..."

    try:

        r = s.get(url, headers=headers, proxies=proxy, verify=True, timeout = 5)

        html = r.content
        #print html
        #print r.history

        redirection_test = len(r.history) > 0 #If the page is being redirected, r.history will contain elements

        if not redirection_test: #apple site is up

            CSRFToken = getString(html, "csrfToken: \"", '\"')
            #print CSRFToken
            return CSRFToken, proxy

        else: #apple site is in maintenance mode

            #print "MAINTENANCE MODE!"
            return "MAINTENANCE", ""

    except:

        #print "ERROR"
        return "ERROR", ""

#Get the wave file url

def getCaptchaData(s, proxy):

    rnd = jsGettime()

    url = 'https://checkcoverage.apple.com/gc?t=audio&timestamp=' + rnd

    headers = {'Accept': '*/*',
                 'Accept-Encoding': 'gzip,deflate,sdch',
                 'Accept-Language': 'en-US,en;q=0.8,fr;q=0.6',
                 'Connection': 'keep-alive',
                 'X-Requested-With': 'XMLHttpRequest',
                 'Host': 'checkcoverage.apple.com',
                 #'Cookie': 's_orientationHeight=693; POD=us~en; s_vnum_n2_us=4|2; s_vi=[CS]v1|2B12C8FB05010C79-600016026009BB12[CE]; s_cc=true; s_fid=362FB5AA2365003B-0D3EA56E7E41217E; s_ppv=acs%253A%253Atools%253A%253Awck%253A%253A1-enter%2520serial%2520number%2520%2528US%2529; s_orientation=%5B%5BB%5D%5D; s_pathLength=support%3D3%2C; s_sq=%5B%5BB%5D%5D',
                 'Referer': 'https://checkcoverage.apple.com/',
                 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36'}

    try:

        r = s.get(url, headers=headers, proxies=proxy, verify=True, timeout = 5)

        try:

            response = r.json()

            requestURL = response['binaryValue']

            return {'requestURL': requestURL,
                     'proxy': proxy}

        except:

            textResponse = r.text
            if textResponse.find('Access Denied') != -1:

                return {'requestURL' : 'Access Denied',
                        'proxy' : ''}

            else:
                #print r.text
                return {'requestURL' : 'Failed to get Captcha',
                        'proxy' : ''}

    except:

        return {'requestURL' : 'Failed to get Captcha',
                'proxy' : ''}


#-------------------------------------------------------------------------------
#Post the guess and the serial number

def postCaptcha(serno, guess, proxy, CSRFToken, s):

    #print "IN POST CAPTCHA"

    url = 'https://checkcoverage.apple.com/us/en/'
    #print url
    payload = {'sno' : str(serno),
                'ans': str(guess),
                 'captchaMode': 'audio',
                'continue': '',
                 'cn': '',
                 'locale': '',
                 'caller': '',
                 'num': '',
                 'CSRFToken': CSRFToken}

    serial = {'sn' : serno}

    headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                 'Accept-Encoding': 'gzip,deflate',
                 'Accept-Language': 'en-US,en;q=0.8,fr;q=0.6',
                 'Connection': 'keep-alive',
                 'Cache-Control': 'max-age=0',
                 'content-length': '129',
                 'content-type': 'application/x-www-form-urlencoded',
                 'Host': 'checkcoverage.apple.com',
                 'Origin': 'https://checkcoverage.apple.com',
                 'Referer': 'https://checkcoverage.apple.com/us/en/?sn=' + serno,
                 #'Cookies': 'ccl=6NFqsqrEHNYupQmz0OL8zg5YM2nmbNfHgi7VbOWgmWmSrAIyf9zaQcovO3TBXcH1e+UVxKhpFhf6Gkfw1NXDg17rcQfmihFzQMGMR3T1B2mvnvGPFpgtUW9tT0IGer0MNfJGyhpOwU3Le0AiU8oN6kiTuxRzUN2SOpCsOB89XmasKr9wkCo/V6anF/zxYt2hqfxgRcR1+ysrESORL7coWiu6MzOrnDdFcTKbhh18gRMphCYuGV/Jkgu8JhYZ7QvhDcHgT90ENrBegIxtwQxMiZOM4it9UHjF8cSVK0m/9tom44e2kUfvVROuWDHC+ea6vKxIT/1ueo8=; geo=US; ac_history=%7B%22search%22%3A%5B%5D%2C%22kb%22%3A%5B%5B%22HT202909%22%2C%22About%20iPhone%20Models%20that%20support%20China%20Mobile%22%2C%22en_US%22%2C1445351457000%2C%22unknown%22%5D%5D%2C%22help%22%3A%5B%5D%2C%22psp%22%3A%5B%5D%2C%22offer_reason%22%3A%7B%7D%2C%22total_count%22%3A%7B%22kbs%22%3A1%2C%22last_kb%22%3A1445351457000%7D%7D; s_cc=true; s_orientation=%5B%5BB%5D%5D; s_pathLength=support%3D1%2C; s_invisit_n2_us=4; s_vnum_n2_us=4%7C9%2C0%7C1; s_vi=[CS]v1|2B12C8FB05010C79-600016026009BB12[CE]; s_ppv=acs%253A%253Atools%253A%253Awck%253A%253A1-enter%2520serial%2520number%2520%2528US%2529%2C65%2C30%2C803%2C300%3A2%7C400%3A%3E%7C500%3A%3E%7C600%3A%3E%7C700%3A%3E; s_orientationHeight=803; s_fid=362FB5AA2365003B-0D3EA56E7E41217E; s_sq=appleussupportdev1%3D%2526pid%253Dacs%25253A%25253Atools%25253A%25253Awck%25253A%25253A1-enter%252520serial%252520number%252520%252528us%252529%2526pidt%253D1%2526oid%253DContinue%2526oidt%253D3%2526ot%253DSUBMIT',
                 'Upgrade-Insecure-Requests': '1',
                 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36'}

    #print "POSTING CAPTCHA ANSWER"

    r = requests.post(url, params=serial, data=payload, headers=headers, cookies=s.cookies, verify=True, proxies=proxy)
    #print r.request.headers
    #print r.url
    #print payload
    #print r.status_code

    debug_mode = False

    if debug_mode:

        file = open('html2.txt')
        html = file.read()

    else:

        html = str(r.content)
        #print html

    ##print html

    if html.find('If this audio is difficult to understand') != -1:

        out = {'test': 'try again',
                'html': ''}

    elif html.find('responseJson') != -1:

        out = {'test': 'exists',
             'html': html}

    elif html.find('this is a serial number for a product that has been replaced') != -1:

        out = {'test': 'replaced',
                 'html': ''}

    elif html.find('this serial number is not valid') != -1:

        #print html

        out = {'test': 'not valid',
             'html': ''}

    #elif html.find('the number you have provided cannot be found in our records') != -1:
    elif html.find('unable to complete your request at this time') != -1 and html.find('errorType: "snError"') != 1:

        #print "snError"

        out = {'test': 'unable',
                 'html': ''}

    elif html.find('unable to complete your request at this time') != -1 and html.find('errorType: "captchaError"') != 1:

        out = {'test': 'try again',
                 'html': ''}

    else:

        #print html

        out = {'test': 'failed',
         'html': ''}

    #print "RESULT: " + out['test']
    return out


def getHTML(serno):

    #print "***************************************************************************************************"
    #print "***************************************************************************************************"
    #print "***************************************************************************************************"

    test = 'try again'
    response = {'test' : '', 'html' : ''}
    attempts = 0

    while test == 'try again' and attempts < 1:

        #print "--------------------------------------------"
        #print "Attempting to get the data"
        attemptStart = time.time()

        with requests.Session() as s:

            attempts += 1

            #print 'Attempt: ' + str(attempts)

            #print "Getting CSRFToken..."

            CSRFToken, proxy = get_csrftoken(s)

            if CSRFToken != "ERROR" and CSRFToken != "MAINTENANCE":

                #print "Getting captcha..."
                start = time.time()

                captcha_test = False
                captcha_tries = 0

                while not captcha_test and captcha_tries < 5:

                    captchaData = getCaptchaData(s, proxy)
                    #print captchaData
                    captcha_test = True if captchaData['requestURL'] != "LTEwMTAgOiBGYXRhbCBJbnRlcm5hbCBlcnJvcg=="  else False
                    captcha_tries += 1
                    #print "Captcha try #", captcha_tries

                end = time.time()
                #print end - start

                if captchaData['requestURL'] == 'Access Denied':

                    response = {'test' :'Access Denied',
                                'html' : ''}
                    test = 'Access Denied'
                    #print test

                elif captchaData['requestURL'] == 'Failed to get Captcha' or captcha_test == False or captchaData['requestURL'] == None:

                    response = {'test' :'failed',
                                'html' : ''}
                    test = 'failed'
                    #print test

                else:

                    url = 'data:audio/wav;base64,' + str(captchaData['requestURL'])

                    captchaGuess = guessFunc(url)

                    #print time.time() - start

                    #print captchaGuess

                    if len(captchaGuess) > 10:

                        #print "Wrong response"
                        response = {'test' : 'failed', 'html' : ''}

                    else:

                        #print "Posting guess & SN..."
                        start = time.time()
                        response = postCaptcha(serno, captchaGuess, captchaData['proxy'], CSRFToken, s)
                        end = time.time()
                        #print end - start

                    test = response['test']

                    attemptEnd = time.time()
                    #print "Time for this attempt:"
                    #print attemptEnd - attemptStart

            elif CSRFToken == "MAINTENANCE":

                response = {'test' : 'maintenance', 'html' : ''}

            else:

                response = {'test' : 'failed', 'html' : ''}

    return response
