from pyAudioAnalysis import audioTrainTest as aT
import wave
import audioop
import urllib
import tempfile
import os
import shutil

def saveSample(wavFile, sample_start, sample_end, samplePath):

    wavFile.setpos(sample_start)
    sample = wavFile.readframes(sample_end - sample_start)
    chunkAudio = wave.open(samplePath, 'w')
    chunkAudio.setnchannels(2)
    chunkAudio.setsampwidth(1)
    chunkAudio.setframerate(8000)
    chunkAudio.writeframes(sample)
    chunkAudio.close()

def sliceCaptcha(tempDir):

    FRAGSIZE = 110
    captchaPath = tempDir + '/captcha.wav'
    samplesPath = tempDir + '/samples/'
    os.mkdir(samplesPath)
    wavFile = wave.open(captchaPath, 'r')
    frameRate = wavFile.getframerate()
    nbFrames = wavFile.getnframes()
    file_nb = 1
    sample_start = 0
    sample_end = 0
    for curPos in xrange(4 * frameRate, nbFrames, FRAGSIZE):
        sliceEnd = curPos + FRAGSIZE
        wavFile.setpos(curPos)
        sampleSlice = wavFile.readframes(FRAGSIZE)
        peak = audioop.maxpp(sampleSlice, 1)
        if peak > 0 and sample_start == 0:
            sample_start = curPos
        if peak == 0 and sample_start != 0:
            sample_end = curPos
        if sample_start != 0 and sample_end != 0:
            samplePath = samplesPath + str(file_nb) + '.wav'
            sample_length = sample_end - sample_start
            if sample_length > 5000:
                saveSample(wavFile, sample_start, sample_end, samplePath)
                file_nb += 1
            sample_start = 0
            sample_end = 0

    if sample_start != 0 and sample_end == 0:
        sample_length = nbFrames - sample_start
        if sample_length > 5000:
            samplePath = samplePath + str(file_nb) + '.wav'
            saveSample(wavFile, sample_start, nbFrames, samplePath)

def recoSamples(tempDir):

    guess = ''
    folderPath = tempDir + '/samples/'
    samples = os.listdir(folderPath)
    index = 0
    for sample in sorted(samples):
        ##print sample
        samplePath = folderPath + sample
        #print samplePath
       	MODEL_PATH = 'knnModel'

       	s = aT.fileClassification(samplePath, MODEL_PATH, "knn")

        classes = ['10', '11', '12', '13', '14', '15', '16', '17', '18',
            '19', '20', '21', '22', '23', '25', '26', '27', '28',
            '29', '30', '31', '32', '33', '35', '36', '37', '38',
            '39', '40', '41', '42', '43', '45', '46', '47', '48',
            '49', '50', '51', '52', '53', '55', '56', '57', '58',
            '59', '60', '61', '62', '63', '65', '66', '67', '68',
            '69', '70', '71', '72', '73', '75', '76', '77', '78',
            '79', '80', '81', '82', '83', '85', '86', '87', '88',
            '89', '90', '91', '92', '93', '95', '96', '97', '98']

        guess += str(classes[s[0]])

        index += 1

    return guess

def guessFunc(url):

	wavFile = urllib.urlopen(url)
	tempDir = tempfile.mkdtemp()

	captchaPath = tempDir + '/captcha.wav'
	output = open(captchaPath, 'wb')
	output.write(wavFile.read())
	output.close()

	sliceCaptcha(tempDir)

	guess = recoSamples(tempDir)

	shutil.rmtree(tempDir)

	return guess
