#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, jsonify, request
from solveCaptcha import getHTML
from activationLock import checkActivationLockStatus
from scrape import processHTML
from getsupport import get_support_data

from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    return 'Hello World! Updated on August 16th 2018'

@app.route('/json/<serial>')
def json_route(serial):
    #print serial
    #Serial comes like this 3577298363228 or like this 3577298363228-1

    items = serial.split('-')

    sn = items[0]

    url = request.url_root
    #url = url.replace('http://', 'https://')

    data = {'serial' : sn,
            'url': url,
            'apple_site_up' : True,
            'failed' : False,
            'SN_exists' : True,
            'SN_type' : 'SERIAL',
            'SN_code' : '',
            'MEID_SN' : '',
            'purchaseDateStatus' : 'registration', #registration, notregistered, iphonenotivated
            'validPurchaseDate' : True,
            'deviceActivated' : True,
            'appleLoaner' : False,
            'replaced' : False,
            'dop': 'Inconnue',
            'warranty_type' : 'Garantie Limitée Apple',
            'phone_support' : 'Expirée',
            'care' : 'Expirée',
            'access_denied' : False,
            'unable' : False,
            'prodDescr':'', #Device's name
            'upgradePlan' : False,
            'captcha' : False,
            'notFound' : False
            }

    #print "GETTING HTML"

    production_mode = True

    if (production_mode):

        response = getHTML(sn)

    else:
        #print "OK"
        with open('test_file.txt', 'r') as f:

            html = f.read()
            ##print html
            response = {'test' : 'exists',
                        'html' : html}

    #print "OK, GOT THE RESPONSE"
    test = response['test']
    html = response['html']

    if test == 'Access Denied':
        #print "IN ACCESS DENIED"
        data['apple_site_up'] = True
        data['access_denied'] = True
        data['captcha'] = True

    elif test == 'maintenance':
        #print "APPLE IS IN MAINTENANCE"
        data['apple_site_up'] = False
        data['access_denied'] = False
        data['captcha'] = False

    elif test == 'failed':
        #print "IN FAILEd"
        data['failed'] = True
        data['access_denied'] = False
        data['captcha'] = False
        #data['unable'] = True
        #print html

    elif test == 'exists':
        #print "IN EXISTS"
        data = processHTML(html, serial, data)

    elif test == 'replaced':
        #print "IN REPLACED"
        data['replaced'] = True
        data['SN_exists'] = True
        data['apple_site_up'] = True
        data['access_denied'] = False
        data['captcha'] = True

    elif test == 'not valid':
        #print "IN NOT VALID"
        data['SN_exists'] = False
        data['apple_site_up'] = True
        data['access_denied'] = False
        data['captcha'] = True

    elif test == 'unable':
        #print "IN NOT FOUND"
        data['SN_exists'] = True
        data['apple_site_up'] = True
        data['access_denied'] = False
        data['captcha'] = True
        data['notFound'] = True
        data['unable'] = True

    else:
        #print "IN CAPTCHA = FALSE"
        data['captcha'] = False

    return jsonify(**data)

@app.route('/light_version/<serial>')
def light_version(serial):
    #print serial
    #Serial comes like this 3577298363228 or like this 3577298363228-1

    items = serial.split('-')

    sn = items[0]

    url = request.url_root
    #url = url.replace('http://', 'https://')

    data = {'serial' : sn,
            'url': url,
            'apple_site_up' : True,
            'failed' : False,
            'SN_exists' : True,
            'SN_type' : 'SERIAL',
            'SN_code' : '',
            'MEID_SN' : '',
            'purchaseDateStatus' : 'registration', #registration, notregistered, iphonenotivated
            'validPurchaseDate' : True,
            'deviceActivated' : True,
            'appleLoaner' : False,
            'replaced' : False,
            'dop': 'Inconnue',
            'warranty_type' : 'Garantie Limitée Apple',
            'phone_support' : 'Expirée',
            'care' : 'Expirée',
            'access_denied' : False,
            'unable' : False,
            'prodDescr':'', #Device's name
            'upgradePlan' : False,
            'captcha' : True,
            'notFound' : False
            }

    #print "GETTING HTML"

    production_mode = True

    if (production_mode):

        response = get_support_data(sn)

    else:
        #print "OK"
        with open('test_file.txt', 'r') as f:

            html = f.read()
            response = {'test' : 'replaced',
                        'html' : html}

    #print "OK, GOT THE RESPONSE"
    test = response['error']


    if response['description'] != 'unknown':

        #print "IN EXISTS"
        data['prodDescr'] = response['description']
        data['care'] = response['warranty_status']
        data['phone_support'] = response['phone_status']
        data['SN_code'] = response['config_code']


    elif response['replaced'] == 'REPLACED':

        #print "IN REPLACED"
        data['replaced'] = True
        data['SN_exists'] = True
        data['apple_site_up'] = True
        data['access_denied'] = False
        data['captcha'] = True

    elif response['invalid_serial'] == 'INVALID':
        #print "IN NOT VALID"
        data['SN_exists'] = False
        data['apple_site_up'] = True
        data['access_denied'] = False
        data['captcha'] = True

    else:
        #print "IN NOT FOUND"
        data['SN_exists'] = True
        data['apple_site_up'] = True
        data['access_denied'] = False
        data['captcha'] = True
        data['notFound'] = True
        data['unable'] = True

    return jsonify(**data)

@app.route('/activation_lock/<serial>')
def icloud(serial):
    #print serial

    items = serial.split('-')
    sn = items[0]

    data = checkActivationLockStatus(sn)

    return jsonify(**data)


@app.route('/favicon.ico')
def favicon():
    return ""

if __name__ == '__main__':
    #app.run(host = '0.0.0.0')
    app.run()
