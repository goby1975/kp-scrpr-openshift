import requests
import json

def get_csrf_token(s):

    url = 'https://getsupport.apple.com/'

    headers = {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Encoding': 'gzip,deflate,sdch',
                'Accept-Language': 'en-US,en;q=0.8,fr;q=0.6',
                'Connection': 'keep-alive',
                'Host': 'getsupport.apple.com',
                'Upgrade-Insecure-Requests':'1',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36',
                'DNT':'1'
                }



    r = s.get(url, headers=headers, cookies=s.cookies)

    #Convert header to lowercase to avoid error

    r_headers = r.headers
    lowercase_headers = {}

    for k, v in r_headers.iteritems():

      lowercase_headers[k.lower()] = v

    return lowercase_headers["x-apple-csrf-token"]


def get_coverage(s, csrf_token, serial):

    csrf_token = get_csrf_token(s)

    #print csrf_token

    url = 'https://getsupport.apple.com/web/v1/coverage'

    headers = {
                'Accept': '*/*',
                'Accept-Encoding': 'gzip,deflate,br',
                'Accept-Language': 'en-US,en;q=0.8,fr;q=0.6',
                'Connection': 'keep-alive',
                'Content-Length':'34',
                'Content-Type': 'application/json; charset=UTF-8',
                'Host': 'getsupport.apple.com',
                'Origin': 'https://getsupport.apple.com',
                'Referer': 'https://getsupport.apple.com/',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36',
                'X-Apple-CSRF-Token': csrf_token,
                'X-Requested-With':'XMLHttpRequest',
                'DNT':'1'
                }

    data = {'serialNumber' : serial}

    jsondata = json.dumps(data)

    r = s.post(url, data=jsondata, headers=headers, cookies=s.cookies)

    return r.json()


def process(data):

    #print "*****************************"
    #print data
    #print "*****************************"

    processed_data = {'description' : 'unknown',
                        'warranty_status' : 'unknown',
                        'phone_status' : 'unknown',
                        'replaced' : "",
                        'invalid_serial' : "",
                        'config_code' : '',
                        'error' : 'NO ERROR'
                        }

    if 'data' in data:

        try:

            processed_data['description'] = data['data']['prodDesc']
            url, processed_data['config_code'] = data['data']['prodImageUrl'].split("=")

        except:

            pass


        processed_data['warranty_status'] = data['data']['supportCoverage']
        processed_data['phone_status'] = data['data']['technicalSupport']


    elif 'errorDetail' in data:

        error_code = data['errorDetail']['errorCode']

        if error_code == 'CASWS-COV-004':

            processed_data['replaced'] = "REPLACED"

        elif error_code == 'CASWS-COV-002':

            processed_data['invalid_serial'] = "INVALID"

        else:

            processed_data['error'] = 'UNKNOWN ERROR'

    else:

        processed_data['error'] = 'OTHER ERROR'

    return processed_data

def get_support_data(serial):

    with requests.Session() as s:

        #print "GETTING CSRF TOKEN..."
        csrf_token = get_csrf_token(s)
        #print "CSFR TOKEN: " + csrf_token

        data = get_coverage(s, csrf_token, serial)

        return process(data)
