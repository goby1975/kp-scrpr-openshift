# coding=utf-8

import requests
import re
import random
from datetime import date, timedelta, datetime
import urllib2
import json
import sys



#**************************************************
#HELPERS
#**************************************************

#Logging

def scraper_log(msg, vble = ''):

    log_line = msg + str(vble) + "\n"

    output_file = open('scraper_log.csv', 'a')

    output_file.write(str(log_line))

    output_file.close()


#Returns a string from html that's located between a front string and an end string
def getString(html, front, end):

    regex = str(front) + '(.+?)' + end
    #print regex
    #scraper_log(regex)
    pattern = re.compile(regex)

    try:

        string = str(re.findall(pattern, html)[0])

    except:

        string = 'Inconnu(e)'

    #Clean string
    string = string.strip()

    return string

#Translate dates to French
def translateDate(date):
    date = str(date)

    fr_en_dates = {'January' : 'Janvier',
                    'February' : 'Février',
                    'March' : 'Mars',
                    'April' : 'Avril',
                    'May' : 'Mai',
                    'June' : 'Juin',
                    'July' : 'Juillet',
                    'August' : 'Août',
                    'September' : 'Septembre',
                    'October' : 'Octobre',
                    'November' : 'Novembre',
                    'December' : 'Décembre'
                    }

    clean_date = date.replace(",", "")

    en_mth, d, y = clean_date.split(' ')

    fr_mth = fr_en_dates[en_mth]

    new_date = d + ' ' + fr_mth + ' ' + y

    return new_date

#Estimate dop given the warranty expiration date and the warranty type
def get_dop(d, w):

    #d is in the english format "December 7, 2016"
    warranty_types = {'Garantie Limitée Apple' : 364,
                'AppleCare+' : 729,
                'AppleCare Protection Plan' : 364}

    try:

        date_obj = datetime.strptime(d , '%B %d, %Y')

    except:

        e = sys.exc_info()[0]
        #print e

    dop = date_obj - timedelta(days = warranty_types[w])

    today = datetime.today()

    """
    Note: the coherence of the guessed dop is processed on Kelpom's server
    while dop > today:

        dop = dop - timedelta(days = 365)
        """

    en_dop = dop.strftime('%B %d %Y')

    fr_dop = translateDate(str(en_dop))

    return fr_dop

#**************************************************
#MAIN FUNCTION
#**************************************************

def processHTML(html, serial, data):

    data['captcha'] = True

    phoneSupportData = []
    serviceCoverageData = []
    #Extract Json from Html

    jsonString = getString(html, r"""Json",""", "}\);") + "}" #we get a str
    #print "1"

    jsonData = json.loads(jsonString) #Convert str to json

    #print jsonData
    #print "2"
    #SAVE ALL THE USEFUL INFO IN VARIABLES

    #LOOP THROUGH THE RESULT LIST AND ASSIGN THE CORRECT DATA TO THE CORRECT VARIABLE

    results = jsonData['results']
    #print results

    appleCareData = {'resultLabel' : ''}

    for item in results:

        if item['resultLabel'].find('Loaner') != -1:
            #print "21"
            data['appleLoaner'] = True

        elif item['resultLabel'].find('Purchase Date') != -1:
            #print "22"
            purchaseDateData = item

        elif item['resultLabel'].find('Telephone Technical Support') != -1:
            #print "23"
            phoneSupportData = item

        elif item['resultLabel'].find('Repairs and Service Coverage') != -1:
            #print "24"
            serviceCoverageData = item

        elif item['resultLabel'].find('AppleCare+') != -1:
            #print "24"
            appleCareData = item

        elif item['resultLabel'].find('AppleCare Protection Plan') != -1:
            #print "24"
            appleCareData = item

        elif item['resultLabel'].find('Please validate') != -1:
            #print "25"
            purchaseDateData = item
            data['validPurchaseDate'] = False

        elif item['resultLabel'].find('Please activate') != -1:
            #print "26"
            purchaseDateData = item
            data['deviceActivated'] = False
    #print "27"

    data['purchaseDateStatus'] = "registration" if not data['appleLoaner'] else ""  #registration, notregistered
    #print "A"
    validationActivation = purchaseDateData['resultLabel'] if not data['appleLoaner'] else "registration" #NEW
    #print "B"
    serialId = jsonData['productInfo']['SERIAL_ID']
    productLabel = jsonData['productInfo']['PRODUCT_LABEL']
    imageUrl = jsonData['productInfo']['PROD_IMAGE_URL'] #Used to get the config code
    data['prodDescr'] = jsonData['productInfo']['PROD_DESCR'] #Product name

    #print "C"

    #Determine type of Serial

    if productLabel == "IMEI:":
        #print "C1"
        data['SN_type'] = "IMEI"

    elif productLabel == "Serial Number:":
        #print "C2"
        data['SN_type'] = "SERIAL"

    else:
        #print "C3"
        data['SN_type'] = "MEID"
        meidUrl = "" #phoneSupportData['buttonLink']

        data['MEID_SN'] = "" #getString(meidUrl, 'sn=', '&')
        #print "C4"
    #print "D"
    #Get the config code
    data['SN_code'] = getString(imageUrl, 'configcode=', '&')

    if data['purchaseDateStatus'] == "registration":

        if len(phoneSupportData) > 0:
            #print "D0"
            phoneSupport = phoneSupportData['resultLabel']
            #print "D1"
            phoneSupportDate = phoneSupportData['resultText'] #Contains the expiration date

        else:

            phoneSupport = ""

        if len(serviceCoverageData) > 0:
            #print "D2"
            repairs = serviceCoverageData['resultLabel']
            #print "D3"
            repairsDetails = serviceCoverageData['resultText'] #Contains details about the warranty

        else:

            repairs = ""

        #print "D4"
        upgradePlan = jsonData['IS_LOANER']
        #print "D5"

        #Apple Upgrade Plan

        data['upgradePlan'] = False if upgradePlan == "N" else True
        #print "E"
        #Telephone Technical Support Info

        if phoneSupport.find('Active') != -1:
            #print "E1"
            string_list = phoneSupportDate.split('<br/>')
            #print "E2"
            for s in string_list:
                #print "E3"
                if s.find('Estimated Expiration Date:') != -1:

                    date_string = s.split(': ')
                    #print date_string[1]
                    data['phone_support'] = translateDate(date_string[1])
                    #print "E4"

                elif s.find("in progress") != -1:

                    data['phone_support'] = "Contrat d'Assistance en cours d'enregistrement"

        #print "F"
        #Warranty

        if repairs.find('Active') != -1:

            if repairsDetails.find('in progress') != -1:

                data['care'] = "Contrat d'Assistance en cours d'enregistrement"
                data['warranty_type'] = 'Garantie Limitée Apple'

            else:

                en_date = getString(repairsDetails, 'Expiration Date: ', '<br/>') #"December 27, 2016"

                data['care'] = translateDate(en_date) #"27 Décembre 2016"

                if appleCareData['resultLabel'].find('AppleCare+') != -1:

                    data['warranty_type'] = 'AppleCare+'

                elif appleCareData['resultLabel'].find('AppleCare Protection Plan') != -1:

                    data['warranty_type'] = 'AppleCare Protection Plan'

                else:

                    data['warranty_type'] = 'Garantie Limitée Apple'

                #Estimate dop

                data['dop'] = get_dop(en_date, data['warranty_type'])

        #print "G"


    #print "H"
    return data
