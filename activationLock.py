from guessCaptcha import guessFunc
import requests
import urllib
import time, datetime
import tempfile
import os
import shutil
import json
import random

#---------------------------------------
#HELPERS
#---------------------------------------

#Corresponding function to Javascript getTime()

def jsGettime():
    rnd = datetime.datetime.now()

    t = str(int(time.mktime(rnd.timetuple())))

    ms = str(int(rnd.microsecond * 0.001))

    return t + ms

#---------------------------------------
#GET THE REQUEST URL AND THE ID TO
#---------------------------------------

def getCaptchaData():

    rnd = jsGettime()

    url = 'https://fmipalcweb.icloud.com/fmipalcservice/client/getCaptchaAudio?rnd=' + rnd

    proxies = [
            {"http": "http://gobajt:DCRz08rp@108.177.131.238:29842"},
            {"http": "http://gobajt:DCRz08rp@108.177.131.66:29842"},
            {"http": "http://gobajt:DCRz08rp@108.177.131.89:29842"},
            {"http": "http://gobajt:DCRz08rp@108.177.131.101:29842"},
            {"http": "http://gobajt:DCRz08rp@108.177.131.121:29842"},
            ]

    proxy = random.choice(proxies)

    headers = {
                'Accept': '*/*',
                'Accept-Encoding': 'gzip,deflate,sdch',
                'Accept-Language': 'en-US,en;q=0.8,fr;q=0.6',
                'Connection': 'keep-alive',
                'content-type': 'text/plain',
                'Host': 'fmipalcweb.icloud.com',
                'Origin': 'https://www.icloud.com',
                'Referer': 'https://www.icloud.com/activationlock/',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36'
                }

    r = requests.post(url,
                 #data=payload,
                 headers=headers,
                 proxies = proxy
                 )



    try:

        response = r.json()

        requestURL = response['audio']
        id =  response['captchaContext']['id']

        return {'requestURL' : requestURL,
            'id' : id,
            'proxy' : proxy
            }

    except:

        return "SERVICE UNAVAILABLE"


def postCaptcha(serial, guess, captchaId, proxy):

    url = 'https://fmipalcweb.icloud.com/fmipalcservice/client/checkActivationLock'

    payload = {'deviceId':serial,'captchaCode':guess,'captchaContext':{'id':str(captchaId)}}

    headers = {
                'Accept': '*/*',
                'Accept-Encoding': 'gzip,deflate',
                'Accept-Language': 'en-US,en;q=0.8,fr;q=0.6',
                'Connection': 'keep-alive',
                'content-length': '1023',
                'content-type': 'text/plain',
                'Host': 'fmipalcweb.icloud.com',
                'Origin': 'https://www.icloud.com',
                'Referer': 'https://www.icloud.com/activationlock/',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36'
                }

    try:

        r = requests.post(url,
                     data=json.dumps(payload),
                     headers=headers,
                     proxies = proxy,
                     timeout = 1
                     )

        response = r.json()

    except:

        response = {'statusCode' : '403'}

    print (response['statusCode'])

    if response['statusCode'] == '403':

        out = "403"

    elif response['statusCode'] == '404':

        out = "404"

    else:

        out = response['locked']


    return out


#----------------------------------------------------
# MAIN
#----------------------------------------------------

def checkActivationLockStatus(serial):


    response = "403"
    attempts = 0
    start = time.time()

    while response == "403":

        attempts += 1

        print ("Attempt: " + str(attempts))

        #Get Captcha URL & ID
        captchaData = getCaptchaData()

        if captchaData == "SERVICE UNAVAILABLE":

            response = "SERVICE UNAVAILABLE"

        else:

            #Use the Captcha Url to get the audio file
            url = captchaData['requestURL']
            print (url[0:100])
            s = time.time()

            captchaGuess = guessFunc(url)

            response = postCaptcha(serial, captchaGuess, captchaData['id'], captchaData['proxy'])

    if response == "404":

        lock_status = "UNSUPPORTED"

    elif response == "SERVICE UNAVAILABLE":

        lock_status = "SERVICE UNAVAILABLE"

    elif response == True:

        lock_status = "LOCKED"

    else:

        lock_status = "UNLOCKED"

    data = {'lock_status' : lock_status}

    return data
